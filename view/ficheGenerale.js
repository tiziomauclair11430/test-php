var compteurEnfant = 0;



// Afficher, cacher les carousels quand on change de menu
var boutonAssuré = document.getElementById("boutonAssuré");
var boutonAssuré2 = document.getElementById("boutonAssuré2");
var boutonEnfant = document.getElementById("boutonEnfant");
var boutonEntreprise = document.getElementById("boutonEntreprise");
var carouselAssuré = document.getElementById('carouselAssuré');
var carouselAssuré2 = document.getElementById('carouselAssuré2');
var carouselEnfant = document.getElementById("carouselEnfant")
var carouselEntreprise = document.getElementById("carouselEntreprise")





boutonAssuré.addEventListener("click", assuréShowCarousel);
function assuréShowCarousel ()
{
    carouselAssuré.classList.remove('ligneCaché');
    carouselAssuré2.classList.add('ligneCaché');
    carouselEnfant.classList.add('ligneCaché');
    carouselEntreprise.classList.add('ligneCaché');
}


boutonAssuré2.addEventListener("click", assuré2ShowCarousel);
function assuré2ShowCarousel ()
{
    carouselAssuré.classList.add('ligneCaché');
    carouselAssuré2.classList.remove('ligneCaché');
    carouselEnfant.classList.add('ligneCaché');
    carouselEntreprise.classList.add('ligneCaché');
}



boutonEnfant.addEventListener("click", enfantShowCarousel);
function enfantShowCarousel ()
{
    carouselAssuré.classList.add('ligneCaché');
    carouselAssuré2.classList.add('ligneCaché');
    carouselEnfant.classList.remove('ligneCaché');
    carouselEntreprise.classList.add('ligneCaché');
}


boutonEntreprise.addEventListener("click", entrepriseShowCarousel);
function entrepriseShowCarousel ()
{
    carouselAssuré.classList.add('ligneCaché');
    carouselAssuré2.classList.add('ligneCaché');
    carouselEnfant.classList.add('ligneCaché');
    carouselEntreprise.classList.remove('ligneCaché');
}










// Recapitulatif infos generales du particulier
var jsonAssuré = {};
var listeInfos = document.getElementById("listeInfos");

var recapInputs = function (){

    while (listeInfos.firstChild) {
        listeInfos.firstChild.remove()
    }
    var inputs = document.getElementsByClassName("entrée")

    jsonAssuré = {};
    var ul = document.createElement("ul");

    for (entrée of inputs) { 
        //console.log(entrée.id);
        //console.log(entrée.value);
        jsonAssuré[entrée.name] = entrée.value; 
        var li = document.createElement("li");
        var name = document.createElement("h3");
        name.innerText = entrée.id;
        li.appendChild(name);
        var value = document.createElement("div");

        if (entrée.value != ""){
            value.innerText = entrée.value;
            li.appendChild(value);
        }
        else 
        {
            value.innerText = "Champ non rempli !";
            value.classList.add('alert');
            value.classList.add('alert-danger');
            value.setAttribute('role', "alert");
            li.appendChild(value);
        }

        ul.appendChild(li);

    }

    listeInfos.appendChild(ul);

}


// Update du tableau infos generales dés qu'on clique sur une fleche du carousel
boutonsUpdate = document.getElementsByClassName("updateRecapitulatif");
for (button of boutonsUpdate){
    button.addEventListener("click", recapInputs );
}








// Assuré 2

var jsonAssuré2 = {};
var listeInfosAssuré2 = document.getElementById("listeInfosAssuré2");

var recapInputsAssuré2 = function (){

    while (listeInfosAssuré2.firstChild) {
        listeInfosAssuré2.firstChild.remove()
    }
    var inputs = document.getElementsByClassName("entréeAssuré2")

    jsonAssuré2 = {};
    var ul = document.createElement("ul");

    for (entréeAssuré2 of inputs) { 
        //console.log(entréeAssuré2.id);
        //console.log(entréeAssuré2.value);
        jsonAssuré2[entréeAssuré2.name] = entréeAssuré2.value; 
        var li = document.createElement("li");
        var name = document.createElement("h3");
        name.innerText = entréeAssuré2.id;
        li.appendChild(name);
        var value = document.createElement("div");

        if (entréeAssuré2.value != ""){
            value.innerText = entréeAssuré2.value;
            li.appendChild(value);
        }
        else 
        {
            value.innerText = "Champ non rempli !";
            value.classList.add('alert');
            value.classList.add('alert-danger');
            value.setAttribute('role', "alert");
            li.appendChild(value);
        }

        ul.appendChild(li);

    }

    listeInfosAssuré2.appendChild(ul);

}


// Update du tableau infos generales dés qu'on clique sur une fleche du carousel
boutonsUpdate = document.getElementsByClassName("updateRecapitulatifAssuré2");
for (button of boutonsUpdate){
    button.addEventListener("click", recapInputsAssuré2 );
}









// enfants
//infosEnfants.children.length

// variables
var listeEnfants = [];
var choisirEnfantAffiché = document.getElementById("choisirEnfantAffiché");
var addEnfant = document.getElementById('addEnfant');
var infosEnfants = document.getElementById("infosEnfants");

// quand on clique sur le bouton
addEnfant.addEventListener("click", ajouterEnfant );
function ajouterEnfant  ()
{
    

    var inputs = document.getElementsByClassName("infosEnfant")

    var listeInputs = {};
    var ul = document.createElement("ul");
    //console.log(inputs);

    // Pour chaque inputs du formulaire enfant
    for (entrée of inputs) { 

        // on la rajoute dans le tableau
        listeInputs[entrée.name] = entrée.value;
        
        var li = document.createElement("li");
        var name = document.createElement("h3");
        // entrée.id
        name.innerText = entrée.name;
        // .replaceAll("_", " ")
        li.appendChild(name);
        var value = document.createElement("div");

        if (entrée.value != ""){
            value.innerText = entrée.value;
            li.appendChild(value);
        }
        else 
        {
            value.innerText = "Champ non rempli !";
            value.classList.add('alert');
            value.classList.add('alert-danger');
            value.setAttribute('role', "alert");
            li.appendChild(value);
        }
        ul.id = "enfant " + ( compteurEnfant) ;
        //console.log(ul.id);
        ul.appendChild(li);
    }

    // cacher l'enfant qu'on vient d'ajouter
    ul.classList.add('ligneCaché');


    listeInputs["identifiantFront"] = compteurEnfant;
    listeEnfants.push(listeInputs);
    infosEnfants.appendChild(ul);
    //console.log(infosEnfants);
    //console.log(listeEnfants);


    // creation d'un bouton pour afficher l'enfant créer
    var button = document.createElement("button"); 
    //button.data = _a;
    button.innerHTML = "Afficher enfant " + ( compteurEnfant + 1) ;
    button.id = "afficher enfant " + ( compteurEnfant) ;
    button.onclick = function(evt)
{

    for(element of infosEnfants.children){
       //console.log(evt);
        if ( element.id == evt.target.id.slice(9)){
            element.classList.remove('ligneCaché');
        }
        else {
            element.classList.add('ligneCaché');
        }
    }
    //ul.classList.remove('ligneCaché');
   //console.log(infosEnfants.children);
    // d'autre trucs pour la recaché aprés ou caché les autres, caché tout les elements d'une classe sauf le ul ?
}


   // creation d'un bouton pour afficher l'enfant créer
   var buttonSupprimer = document.createElement("button"); 
   //button.data = _a;
   buttonSupprimer.innerHTML = "Supprimer enfant " + ( compteurEnfant + 1) ;
   buttonSupprimer.id = "supprimer enfant " + ( compteurEnfant) ;
   buttonSupprimer.onclick = function(evt)
{

   for(element of infosEnfants.children){
      //console.log(evt);
       if ( element.id == evt.target.id.slice(10)){

        for(ligne of listeEnfants){
            console.log(ligne["identifiantFront"]);
            console.log(evt.target.id.slice(17));
            console.log(listeEnfants.indexOf(ligne));
            if (ligne["identifiantFront"] == evt.target.id.slice(17)){
                listeEnfants.splice(listeEnfants.indexOf(ligne), 1);
            }
        }
    
           element.remove();
           evt.target.remove();
           document.getElementById("afficher " + element.id).remove();
       }
       else {
          
       }
   }
   //ul.classList.remove('ligneCaché');
  //console.log(infosEnfants.children);
   // d'autre trucs pour la recaché aprés ou caché les autres, caché tout les elements d'une classe sauf le ul ?
}


choisirEnfantAffiché.appendChild(button);
choisirEnfantAffiché.appendChild(buttonSupprimer);
compteurEnfant++;
console.log(listeEnfants);
    // var choisirEnfantAffiché = document.getElementById("choisirEnfantAffiché");
}   









// Modif infos enfants
var updateEnfant = document.getElementById("updateEnfant");
updateEnfant.addEventListener("click", modifEnfant );
function modifEnfant  ()
{

    // numero = 10, truc a modif = nom, valuemodif = toto
    // id = 9 

    var numéro = document.getElementById("inputNuméro");
    var trucAModif = document.getElementById("trucAModif")
    var valueModif = document.getElementById("valueModif")
    console.log(numéro);
    // listeEnfants[numéro.value][trucAModif.value] = valueModif.value;
    for(ligne of listeEnfants){
        if (ligne["identifiantFront"] == (numéro.value - 1)){
            ligne[trucAModif.value] = valueModif.value;
        }
    }
    for(ul of infosEnfants.children){
        console.log(ul.id);
        console.log(numéro);
         if ( ul.id == "enfant " + ( numéro.value - 1) ){
             // on a le bon ul
             for (li of ul.children) {
                 if (li.children[0].innerHTML == trucAModif.value)
                 {
                    li.children[1].innerHTML = valueModif.value;
                    li.children[1].className = "";
                 }
                //console.log(li.children[1].innerHTML);
             }
         }

    console.log(listeEnfants);
    //afficherTableau();
}
}








// Recapitulatif infos generales du particulier
var jsonEntreprise = {};
var listeInfosEntreprise = document.getElementById("listeInfosEntreprise");

var recapInputsEntreprise = function (){

    while (listeInfosEntreprise.firstChild) {
        listeInfosEntreprise.firstChild.remove()
    }
    var inputs = document.getElementsByClassName("infosEntreprise")

    jsonEntreprise = {};
    var ul = document.createElement("ul");

    for (entrée of inputs) { 
        //console.log(entrée.id);
        //console.log(entrée.value);
        jsonEntreprise[entrée.name] = entrée.value; 
        var li = document.createElement("li");
        var name = document.createElement("h3");
        name.innerText = entrée.id;
        li.appendChild(name);
        var value = document.createElement("div");

        if (entrée.value != ""){
            value.innerText = entrée.value;
            li.appendChild(value);
        }
        else 
        {
            value.innerText = "Champ non rempli !";
            value.classList.add('alert');
            value.classList.add('alert-danger');
            value.setAttribute('role', "alert");
            li.appendChild(value);
        }

        ul.appendChild(li);

    }

    listeInfosEntreprise.appendChild(ul);

}


// Update du tableau infos generales dés qu'on clique sur une fleche du carousel
boutonsUpdateEntreprise = document.getElementsByClassName("updateRecapitulatifEntreprise");
for (button of boutonsUpdateEntreprise){
    button.addEventListener("click", recapInputsEntreprise );
}





// JSON





document.getElementById("testjson").addEventListener("click", async function () {


    // assuré

    let data = [];
    data.push(jsonAssuré);
    data.push(jsonAssuré2)
    data.push(listeEnfants);
    data.push(jsonEntreprise);
    // ../control/controlAssuréMobile.php
    let sendresponseAssuré = await fetch('../control/controlAssuréMobile.php', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
    .then(r => {
        if (r.code = 200){
            console.log("ok");
        }
    })
    ;

    //window.location = "up.php";
});










// carouselAssuré.classList.remove('ligneCaché');
// carouselEnfant.classList.add('ligneCaché');




/* 
var carousel = document.getElementById('carouselAssuréInner')
    var page = document.createElement("div");
    page.classList.add('carousel-item');
    
    var textnode = document.createTextNode("Water");  // Create a text node
    console.log(carousel);
    console.log(page);
    page.appendChild(textnode);         
    //carousel.insertBefore(page, carousel.childNodes[0]);  // Insert <li> before the first child of <ul> 
    carousel.appendChild(page);
    */